<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = [
        'message',
        'friend_id',
        'user_id'
    ];



    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
