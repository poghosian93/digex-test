<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['body', 'avatar'];
    protected $guardedp = [];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment', 'status_id');
    }

}
