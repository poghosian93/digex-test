<?php

namespace App\Models;

use Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getName(){
        if ($this->name){
            return $this->name;
        }
        return null;
    }

    public function getAvatarUrl(){
        return "https://www.gravatar.com/avatar/{{ md5($this->email)?d=mp&s=200 }}";
    }

    # status
    public function statuses(){
        return $this->hasMany('App\Models\Status', 'user_id');
    }

    #MESSAGES

    public function messages($friend_id){
        $ff = $this->hasMany('App\Models\Messages','friend_id')->orWhere('friend_id',  $friend_id)->get();
        $coll = collect();
        foreach($ff as $f){
                if ($f->user_id === $friend_id || $f->friend_id === $friend_id){
                    if ($f->user_id === Auth::user()->id || $f->friend_id === Auth::user()->id){
                        $coll->push($f);
                    }
            }
        }
        return $coll;
    }
    #comment

//    public function comments(){
//        return $this->hasMany('App\Models\Comment', 'user_id');
//    }

    public function getAvatarsPath($user_id){
        $path = "uploads/avatars/id{$user_id}";
        if (! file_exists($path)){
            mkdir($path, 0777, true);
        }
            return "/$path/";

    }

    public function clearAvatars($user_id){
       $us = User::where('id', $user_id)->first();
        $path = "/uploads/avatars/id{$user_id}";

        if (file_exists(public_path("/$path"))){
            foreach (glob(public_path("/$path/*")) as $avatar){
                if ($avatar == public_path('/'.$path.'/'.$us->avatar)){
                    unlink($avatar);
                }
            }
        }
    }
    public function clearAvatarsName($av_name){
        $us_id =Auth::user()->id;
        $path = "uploads/avatars/id{$us_id}";

        if (file_exists(public_path("/$path"))){
            foreach (glob(public_path("/$path/*")) as $avatar){

                if ($avatar == public_path('/'.$path.'/'.$av_name)){
                    unlink($avatar);
                }

            }
        }
    }

    public function friendsOfMine(){
        return $this->belongsToMany('App\Models\User', 'friend', 'user_id', 'friend_id');
    }

    public function friendOf(){
        return $this->belongsToMany('App\Models\User', 'friend', 'friend_id', 'user_id');
    }

    public function friends(){
       return $this->friendsOfMine()->wherePivot('accepted', true)->get()
            ->merge($this->friendOf()->wherePivot('accepted', true)->get() );
    }

    public function friendRequests(){
        return $this->friendsOfMine()->wherePivot('accepted', false)->get();
    }

    #pending request
    public function friendRequestsPending(){
        return $this->friendOf()->wherePivot('accepted', false)->get();
    }

    #have a friend request
    public function hasFriendRequestPending(User $user){
        return (bool) $this->friendRequestsPending()->where('id', $user->id)->count();
    }

    #get friend request
    public function hasFriendRequestReceived(User $user){
        return (bool) $this->friendRequests()->where('id', $user->id)->count();
    }

    #add friend
    public function addFriend(User $user){
        $this->friendOf()->attach($user->id);
    }

    #remove from friends
    public function deleteFriend(User $user){
        $this->friendOf()->detach($user->id);
        $this->friendsOfMine()->detach($user->id);
    }

    #accept friend request
    public function acceptFriendRequest(User $user){
        $this->friendRequests()->where('id', $user->id)->first()->pivot->update([
            'accepted' => true
        ]);
    }

    #the user is already friends
    public function isFriendWith(User $user){
        return (bool) $this->friends()->where('id', $user->id)->count();
    }


//    public function messagesOfMine(){
//        return $this->belongsToMany('App\Models\User', 'messages', 'user_id', 'friend_id');
//    }
//
//    public function messagesOf(){
//        return $this->belongsToMany('App\Models\User', 'messages', 'friend_id', 'user_id');
//    }
//    public function messages(){
//        return $this->messagesOfMine()->wherePivot('user_id', Auth::user()->id)->get()
//            ->merge($this->messagesOf()->wherePivot('friend_id', Auth::user()->id)->get() );
//    }
}
