<?php

namespace App\Http\Controllers;
use App\Models\Status;
use Auth;
use Image;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    public function getProfile($name){
        $user = User::where('name', $name)->first();
        if (!$user){
            abort(404);
        }
        return view('profile.index', compact('user'));
    }

    public function getAllProfile(){
        $users = User::all();
        return view('profile.allProfile', compact('users'));
    }

    public function getEdit(){

        return view('profile.edit');
    }

    public function postEdit(Request $req){
        $this->validate($req,[
            'name' =>'alpha|max:50'
        ]);

        Auth::user()->update([
            'name' => $req->input('name')
        ]);

        return redirect()->route('profile.edit')->with('info', 'profiles will update');

    }

    public function postUploadAvatar(Request $req, $name){
        $user = User::where('name', $name)->first();
        if (!Auth::user()->id === $user->id){
            return redirect()->route('home');
        }

        if ($req->hasFile('avatar'))
        {

            $user->clearAvatars($user->id);

            $avatar = $req->file('avatar');

            $filename = time() . '.' . $avatar->getClientOriginalExtension();

              Image::make($avatar)->resize(300,300)
               ->save( public_path( $user->getAvatarsPath($user->id)) . $filename);
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();

            return redirect()->back();
        }
    }
    public function getDeleteAvatar($user_id){
        Auth::user()->clearAvatars($user_id);
        Auth::user()->avatar = null;
        Auth::user()->save();

        return redirect()->route('profile.index', ['name' => Auth::user()->name]);
    }


}
