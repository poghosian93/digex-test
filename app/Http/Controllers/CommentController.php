<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Status;
use App\Models\Comment;
use Auth;

class CommentController extends Controller
{
    public function postReply(Request $req, $statusId){
        $this->validate($req, [
            "reply-{$statusId}" => 'required|max:1000'
        ]);
        $stat = Status::find($statusId);

        if (!$stat) redirect()->route('home');

            $stat->comments()->create([
                'body' => $req->input("reply-{$statusId}"),
                'user_id' => Auth::user()->id
            ]);

        return redirect()->back();
    }

    public function postSend(Request $req, $statusId, $reply = null){

        $this->validate($req, [
            "reply" => 'required|max:1000'
        ]);
        $stat = Status::find($statusId);

        if (!$stat) redirect()->route('home');

            $stat->comments()->create([
                'body' => $req->input("reply"),
                'user_id' => Auth::user()->id,
                'reply_id' => $reply
            ]);

        return redirect()->back();
    }


}
