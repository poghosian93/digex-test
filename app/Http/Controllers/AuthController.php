<?php

namespace App\Http\Controllers;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\SigninRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Status;
use Auth;

class AuthController extends Controller
{
    public function getSignup(){
        return view('auth.signup');
    }

    public function postSignup(AuthRequest $req){
        $user = User::create([
            'email' =>$req->input('email'),
            'name' =>$req->input('name'),
            'password' =>bcrypt($req->input('password'))
        ]);
        return redirect()->route('home')->with('info', 'you have successfully registered');
    }

    public function getSignin(){
        return view('auth.signin');
    }

    public function postSignin(SigninRequest $req){
        if (!Auth::attempt($req->only(['email', 'password']))){
            return redirect()->back()->with('info','Wrong password');
        }else{
            return redirect()->route('home')->with('info', "Hi ". Auth::user()->name);
        }

    }
    public function getSignout(){
        Auth::logout();
        return redirect()->route('home');
    }
}
