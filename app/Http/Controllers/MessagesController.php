<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Messages;
use Illuminate\Mail\Message;

class MessagesController extends Controller
{
    public function postSendMessage(Request $req, $friend_id){

//        $this->validate($req,[
//            'message' => 'required|max:1000|min:1'
//        ]);
      Messages::create([
          'user_id' => Auth::user()->id,
          'friend_id' => $friend_id,
          'message' => $req->input('message')
      ]);
            return redirect()->back();
    }
}
