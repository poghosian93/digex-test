<?php

namespace App\Http\Controllers;

use Auth;
use Image;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Status;

class StatusController extends Controller
{
    public function postStatus(Request $req){
        $this->validate($req,[
                'status' => 'required|max:1000|min:1'
            ]);

        if ($req->file('avatar')){
            $avatar = $req->file('avatar');

            $filename = time() . '.' . $avatar->getClientOriginalExtension();

            Image::make($avatar)->resize(300,300)
                ->save( public_path( Auth::user()->getAvatarsPath(Auth::user()->id)) . $filename);
            Auth::user()->statuses()->create([
                'body' => $req->input('status'),
                'avatar' => $filename
            ]);
        }else{
            Auth::user()->statuses()->create([
                'body' => $req->input('status'),
            ]);
        }



        return redirect()->route('home')->with('info', 'status will share');
    }

    public function getDeleteStatus($status_id){
        $st = Status::where('id', $status_id)->first();
        Auth::user()->clearAvatarsName($st->avatar);

       Status::where('id', $status_id)->delete();

        return redirect()->back();
    }
}
