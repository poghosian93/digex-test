<?php

namespace App\Http\Controllers;
use DB;
use App\Models\User;
use App\Models\Status;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function getResult(Request $req){
        $query = $req->input('query');
        if (!$query){
            redirect()->route('home');
        }

        $users = User::where('name', $query)->first();



        return view('search.results' , compact('users'));
    }
}
