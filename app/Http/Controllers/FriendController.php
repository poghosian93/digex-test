<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function getIndex(){
        $friend =Auth::user()->friends();
        $request = Auth::user()->friendRequests();

        return view('friends.index', [
            'friend' => $friend,
            'request' => $request
        ]);
    }

    public function getAdd($name){
        $user = User::where('name', $name)->first();

        if(!$user){
            return redirect()->back()->with('info', 'User is not found');
        }

        if (Auth::user()->id === $user->id) return redirect()->back();

        if (Auth::user()->hasFriendRequestPending($user) || $user->hasFriendRequestPending(Auth::user())){
            return redirect()
            ->route('profile.index', ['name' => $user->name])
                ->with('info', 'a friend request has been sent to the user');
        }

        if (Auth::user()->isFriendWith($user)){
            return redirect()
                ->route('profile.index', ['name' => $user->name])
                ->with('info', 'the user is already friends');
        }

        Auth::user()->addFriend($user);

        return redirect()
            ->route('profile.index', ['name' => $name])
            ->with('info', 'a friend request has been sent to the user');

    }

    public function getAccept($name){
        $user = User::where('name', $name)->first();

        if(!$user){
            return redirect()->back()->with('info', 'User is not found');
        }

        if (! Auth::user()->hasFriendRequestReceived($user)){
            return redirect()->back();
        }

        Auth::user()->acceptFriendRequest($user);

        return redirect()
            ->route('profile.index', ['name' => $name]  )
            ->with('info', 'friend requests accepted');

    }
    public function postDelete($name){
        $user = User::where('name', $name)->first();

        if (! Auth::user()->isFriendWith($user)){
            return redirect()->back();
        }
        Auth::user()->deleteFriend($user);
        return redirect()->back()->with('info', 'you have removed from friends '.$name);
    }
}
