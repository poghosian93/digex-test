@extends('layouts.app')

@section('title')
    Profile
@endsection

@section('content')
            <div class="">
                    @include('user.partials.userblock')
            </div>
@endsection
