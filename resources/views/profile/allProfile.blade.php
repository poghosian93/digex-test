@extends('layouts.app')

@section('title')
    All Profile
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            @foreach($users as $user)
                        @include('user.partials.alluser')
            @endforeach
        </div>
    </div>
@endsection
