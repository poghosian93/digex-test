@extends('layouts.app')

@section('title')
    All Profile
@endsection

@section('content')
    @if($errors->any())
        <div class="alert alert-danger col-lg-6">
            <ul>
                @foreach($errors->all() as $er)
                    <li>
                        {{$er}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <form method="post" action="{{route('profile.edit')}}" novalidate>
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                           id="name" value="{{ Request::old('name') ?: Auth::user()->name }}">
                </div>

                <button type="submit" class="btn btn-primary">ubdate proiles</button>
            </form>
        </div>
    </div>
@endsection
