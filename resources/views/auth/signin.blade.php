@extends('layouts.app')


@section('title')
    Registr
@endsection

@section('content')

    <div class="row padtb">
        <div class="col-lg-6 auto login_style">
            <h1>Registr</h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $er)
                            <li>
                                {{$er}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{route('auth.signin')}}" novalidate>
                @csrf
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" value="{{ Request::old('email') ? : '' }}"
                           class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}"
                           id="password" placeholder="Password">
                </div>
                <button type="submit" class="btn sibtn btn-outline-primary mb-3">Sign in</button>
            </form>
        </div>
    </div>

@endsection

