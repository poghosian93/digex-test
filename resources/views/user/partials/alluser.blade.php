<div class="media mb-3">
    {{--    <a href="{{ route('profile.index', ['name' => $user->name]) }}">--}}
    <a href="{{ route('profile.index', ['name' => $user->name]) }}">
        @if(! $user->avatar)
            <img style="border-radius: 50%; width: 100px;" src="{{ $user->getAvatarUrl() }}" class="mr-3" alt="{{ $user->getName() }}">
        @else
            <img style="border-radius: 50%; width: 100px;" src="{{ $user->getAvatarsPath($user->id) . $user->avatar }}" class="mr-3" alt="{{ $user->getName() }}">
        @endif
{{--        <img src="{{ $user->getAvatarUrl() }}" class="mr-3" alt="{{ $user->getName() }}">--}}
    </a>
    <div class="media-body">
        <h5 class="mt-0"><a href="{{ route('profile.index', ['name' => $user->name]) }}">{{ $user->getName() }}</a></h5>
    </div>
</div>



