<div class="">
    <div class="d-flex justify-content-between align_end">
        <div class="img_block">
            <a href="{{ route('profile.index', ['name' => $user->name]) }}">
                @if(! $user->avatar)
                        <img src="{{ $user->getAvatarUrl() }}" class="ava_img" alt="{{ $user->getName() }}">
                @else
                    @if(Auth::user()->avatar)
                        <a style="position: absolute;" href="{{ route('profile.avatarDelete', ['user_id' => $user->id]) }}"><i class="fas fa-times"></i></a>
                    @endif
                    <img src="{{ $user->getAvatarsPath($user->id) . $user->avatar }}" class="ava_img" alt="{{ $user->getName() }}">
                @endif
            </a>
            <div class="media-body">
                <h5 class="mt-0"><a href="{{ route('profile.index', ['name' => $user->name]) }}">{{ $user->getName() }}</a></h5>
                @if(Auth::user()->id === $user->id)
                    <div>
                        <form action="{{ route('upload.avatar', ['name' => Auth::user()->name]) }}" method="POST" enctype="multipart/form-data" class="my-4">
                            @csrf
                            <div class="d-flex justify-content-center">
                                <label for="avatar"></label>
                                <input class="down_button" type="file" name="avatar" id="avatar">
                                <div>
                                    <input type="submit" class="btn btn_black pp_3" value="Download">
                                </div>
                            </div>
                        </form>
                        <hr>
                    </div>
                @endif
                @if(Auth::user()->isFriendWith($user))
                    <button class="messa btn-primary btn mb-3"> send messages</button>
                        <div class="mes_block d-none ">
                            <form action="{{ route('messages.send', ['friend_id'=> $user->id]) }}" method="POST">
                                @csrf
                                <div class="mb-3 messig_block position-relative ">
                                    <i class="fas fa-times xiki_st xik"></i>
                                    @if(Auth::user()->messages($user->id))
                                        {{ $name = "" }}
                                        {{ $cl = "" }}
                                        @foreach(Auth::user()->messages($user->id) as $key=> $mes)
                                            <div class="mb-2 back_col_mes">
                                                @if($key === 0)
                                                    <h6>{{ $mes->user->name }}</h6>
                                                @endif
                                                @if($mes->user->name !== $name && $name !== "")
                                                    <h6>{{ $mes->user->name }}</h6>
                                                @endif
                                                <p class="text-monospace mb_m3">{{ $mes->message }}</p>
                                                <p class="emoji mb_m3 font_10 black">{{ $mes->created_at->diffForHumans() }}</p>
                                                <span class="d-none">{{$name = $mes->user->name}}</span>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                                <input class="d-block mesige_inp" name="message" placeholder="write">
                                <input type="submit" class="btn btn-primary">
                            </form>
                        </div>
                @endif

            </div>
        </div>
        @include('inc.statuses')

        <div style="">
            @if(Auth::user()->hasFriendRequestPending($user))
                    <p>pending {{ $user->getName() }} friend confirmation</p>
                @elseif(Auth::user()->hasFriendRequestReceived($user))
                    <a href="{{ route('friend.accept', ['username' => $user->name]) }}" class="btn btn-primary">confirm friendship</a>
                @elseif(Auth::user()->isFriendWith($user))
                    {{ $user->getName() }} in your friends
                    <form method="POST" action="{{ route('friend.delete', ['username' => $user->name]) }}">
                        @csrf
                        <input type="submit" class="btn btn-primary mb-3 mt-3" value="remove from friends">
                    </form>
                @else
                    @if(Auth::user()->id !== $user->id)
                         <a href="{{ route('friend.add', ['username' => $user->name]) }}" class="btn btn-primary">add to friends list</a>
                        @endif
            @endif

            <p> <span style="font-weight: bold;">{{ $user->getName() }} </span>  Friends.</p>

            @if(!$user->friends()->count())
                <p>no friends</p>
            @else
                <div class="friends_block">
                    @foreach($user->friends() as $us)
                        <a href="{{ route('profile.index', ['name' => $us->name]) }}">
                            <div class="mb-3 ac_st d-flex align-items-center">
                                    @if(! $us->avatar)
                                        <img style="border-radius: 50%; width: 50px;" src="{{ $us->getAvatarUrl() }}" class="mr-3" alt="{{ $us->getName() }}">
                                    @else
                                        <img style="border-radius: 50%; width: 50px;" src="{{ $us->getAvatarsPath($us->id) . $us->avatar }}" class="mr-3" alt="{{ $us->getName() }}">
                                    @endif
                                    <span class="d-block">{{ $us->name }}</span>
                            </div>
                        </a>

                    @endforeach
                </div>
            @endif
    </div>
    </div>




    {{--@foreach($collection[1] as $status)--}}
    {{--    <p>{{ $status->body }}</p>--}}
    {{--    @endforeach--}}
</div>


