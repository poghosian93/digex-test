@extends('layouts.app')

@section('title')
    404
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        <div>
            <h1>error 404</h1>
            <h2>back to the main page</h2>
            <h1><a href="{{ route('home') }}">Home</a></h1>
        </div>
    </div>
@endsection
