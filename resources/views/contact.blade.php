@extends('layouts.app')


@section('title')
    contact
@endsection

@section('content')
    <h1>contact</h1>

    @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $er)
                        <li>
                            {{$er}}
                        </li>
                        @endforeach
                </ul>
            </div>
        @endif

    <form action="{{route('contact-form')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Write a name</label>
            <input type="text" name="name" placeholder="Write a name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">write a name</label>
            <input type="text" name="email" placeholder="email" id="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="subject">thems message</label>
            <input type="text" name="subject" placeholder="theme" id="subject" class="form-control">
        </div>
        <div class="form-group">
            <label for="message">message</label>
            <textarea name="message" id="message" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-success">send</button>
    </form>
@endsection
