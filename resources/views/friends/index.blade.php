@extends('layouts.app')

@section('title')
    Friends
@endsection

@section('content')
    <div class="d-flex justify-content-between">

        <div>
            <h3>Your friends</h3>
            @if(!$friend->count())
                <p>no friends</p>
            @else
                <div class="d-flex flex-wrap">
                    @foreach($friend as $us)
                        <a class="friend_marg" style="position: relative;" href="{{ route('profile.index', ['name' => $us->name]) }}">
                            <div  style="background-color: white; border-radius: 5%; width: 250px; padding: 10px;">
                                    @if(! $us->avatar)
                                        <img style="border-radius: 50%; width: 50px;" src="{{ $us->getAvatarUrl() }}" class="mr-3" alt="{{ $us->getName() }}">
                                    @else
                                        <img style="border-radius: 50%; width: 50px;" src="{{ $us->getAvatarsPath($us->id) . $us->avatar }}" class="mr-3" alt="{{ $us->getName() }}">
                                    @endif
                                    <span style="color: black;">{{ $us->name }}</span>
                            </div>
                        </a>
                    @endforeach
                </div>

            @endif
        </div>
        <div>
            <h3>Friend requests</h3>
            @if(!$request->count())
                <p>no requests</p>
            @else
                <div class="d-flex flex-wrap">
                    @foreach($request as $us)
                        <a class="friend_marg" style="position: relative;" href="{{ route('profile.index', ['name' => $us->name]) }}">
                            <div  style="background-color: white; border-radius: 5%; width: 250px; padding: 10px;">
                                @if(! $us->avatar)
                                    <img style="border-radius: 50%; width: 50px;" src="{{ $us->getAvatarUrl() }}" class="mr-3" alt="{{ $us->getName() }}">
                                @else
                                    <img style="border-radius: 50%; width: 50px;" src="{{ $us->getAvatarsPath($us->id) . $us->avatar }}" class="mr-3" alt="{{ $us->getName() }}">
                                @endif
                                <span style="color: black;">{{ $us->name }}</span>
                            </div>
                        </a>
                    @endforeach
                </div>

            @endif
        </div>
    </div>
    @endsection
