@extends('layouts.app')


@section('title')
    Search
@endsection

@section('content')
    <h3>result search "{{ Request::input('query') }}" </h3>
    @if(!$users)
        <p>User is not found</p>
    @else
        <div class="row">
            <div class="col-lg-6">
                    @include('inc.searchres')
            </div>
        </div>
    @endif
@endsection
