<div class="">
    <div class="">
        @if(! $user->statuses->count())
            <p>poka net zapisei</p>
        @else
            @foreach($user->statuses as $status)
                <div style="width: 930px;" class=" mb_70">
                    <a class="mr-3" href="{{ route('profile.index', ['name'=> $status->user->name]) }}">
                        <div class="small_img">
                            @if(! $user->avatar)
                                    <img src="{{ $user->getAvatarUrl() }}" class="sm_im" alt="{{ $user->getName() }}">
                            @else
                                    <img src="{{ $user->getAvatarsPath($user->id) . $user->avatar }}" class="sm_im" alt="{{ $user->getName() }}">
                            @endif
                                     <h2 class="black">{{$status->user->getName() }}</h2>
                        </div>
                    </a>
                    <div class="media-body">


                        <div class="status_block">
                            @if(Auth::user())
                                <a title="clear" class="del_cl" href="{{ route('status.delete', ['status_id' => $status->id]) }}"><i class="fas fa-times"></i></a>
                            @endif
                            <p class="status_body">{{ $status->body }}</p>
                            @if($status->avatar)
                                <div>
                                    <img style="width: 300px"  src="{{ $status->user->getAvatarsPath($status->user->id) . $status->avatar }}" class="mr-3 stat_img" alt="{{ $status->user->getName() }}">
                                </div>
                            @endif
                            <p class="font_12">{{ $status->created_at->diffForHumans() }}</p>
                            <hr>
                            <form method="POST" action="{{ route('comment.post', ['statusId' => $status->id]) }}" class="mb-4">
                                @csrf
                                <div class="form-group">
                                    <textarea rows="3" placeholder="comment" name="reply-{{ $status->id }}" class="area_style form-control mb-2
                                                {{ $errors->has("reply-{$status->id}") ? 'is-invalid' : '' }}"></textarea>
                                    @if($errors->has("reply-{$status->id}"))
                                        <div class="invalid-feedback">
                                            {{ $errors->first("reply-{$status->id}") }}
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn_black mr-5">Comment</button>
                                    @if($status->comments->count())
                                        <a href="#" class="all_com ">all comment  ({{$status->comments->count()}}) ... </a>
                                    @endif
                                </div>
                            </form>
                            <hr>
                                <div class="comments_block ">
                                    @foreach($status->comments as $com)

                                        @if($com->status_id == $status->id && $com->reply_id == null)
                                            <div class="stat_back_col">
                                                <hr>
                                                <div class="d-flex">
                                                    <a class="mr-3" href="{{ route('profile.index', ['name'=> $com->user->name]) }}">
                                                        @if(! $com->user->avatar)
                                                            <img  src="{{ $com->user->getAvatarUrl() }}" class="mr-3 com_ava" alt="{{ $com->user->getName() }}">
                                                        @else
                                                            <img  src="{{$com->user->getAvatarsPath($com->user->id) . $com->user->avatar }}" class="mr-3 com_ava" alt="{{ $com->user->getName() }}">
                                                        @endif
                                                    </a>
                                                    <div>
                                                        <a href="{{ route('profile.index', ['name'=> $com->user->name]) }}" class="font_24">{{ $com->user->name }}</a>
                                                        <h2>{{ $com->body }}</h2>
                                                        <sapn class="font_12">{{ $com->created_at->diffForHumans() }}</sapn>
                                                    </div>
                                                </div>
                                                <hr>
                                                @foreach($status->comments as $co)
                                                    @if($com->id == $co->reply_id)
                                                        <div class="d-flex mb-3">
                                                            <a class="mr-3" href="{{ route('profile.index', ['name'=> $com->user->name]) }}">
                                                                @if(! $co->user->avatar)
                                                                    <img  src="{{ $co->user->getAvatarUrl() }}" class="mr-3 com_ava" alt="{{ $co->user->getName() }}">
                                                                @else
                                                                    <img  src="{{$co->user->getAvatarsPath($co->user->id) . $co->user->avatar }}" class="mr-3 com_ava" alt="{{ $co->user->getName() }}">
                                                                @endif
                                                            </a>
                                                            <div>
                                                                <p><a href="{{ route('profile.index', ['name'=> $co->user->name]) }}">{{ $co->user->name }} </a>Replied to comment</p>
                                                                <p style="display: block;" href="#">{{ $co->body }}</p>
                                                                <sapn class="font_12">{{ $co->created_at->diffForHumans() }}</sapn>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endif
                                                @endforeach
                                                <form method="POST" class="forms_open" action="{{ route('comment.otvet',['statusId' => $status->id,'reply' => $com->id]) }}">
                                                    @csrf
                                                    <input class="form-control mb-2" name="reply" placeholder="Reply">
                                                    <button type="submit" class="btn btn_black">Reply</button>
                                                </form>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
