<header class="d-flex header flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 border-bottom shadow-sm">
    <a href="/" class="my-0 mr-md-auto font-weight-normal"><h2 class="col_white">Test-<img width="100px" src="/images/dd.j.jpg"></h2></a>

    @if(Auth::check())
        <nav class="my-2 my-md-0 mr-md-3 mr-3">

            <a class="p-2 col_white" href="/">Home</a>
            <a class="p-2 col_white" href="/about">About</a>
            <a class="p-2 col_white" href="/contact">Contact</a>
            <a class="p-2 col_white" href="{{ route('profile.allProfile') }}">All users</a>
            <a class="p-2 col_white" href="{{ route('friend.index') }}">Friends</a>
        </nav>
        <form method="GET" action="{{ route('search.results') }}" class="mr-5 form-inline my-2 ml-2 my-lg-0">
            <input name="query" class="form-control mr-sm-2" type="search" placeholder="whom find?" aria-label="Search">
            <button type="submit"  class="btn d-none btn_black my-2 my-sm-0">Search</button>
        </form>
            @if(! Auth::user()->avatar)
                    <a class="mr-2" href="{{ route('profile.index', ['name' => Auth::user()->name]) }}"><img style="width: 50px; border-radius: 50%;" src="{{ Auth::user()->getAvatarUrl() }}"></a>
              @else
                    <a class="mr-2" href="{{ route('profile.index', ['name' => Auth::user()->name]) }}"><img style="width: 50px; border-radius: 50%;" src="{{ Auth::user()->getAvatarsPath(Auth::user()->id) . Auth::user()->avatar }}"></a>

                @endif
        <a class="mr-3 font_24 col_white" href="{{ route('profile.index', ['name' => Auth::user()->name]) }}">{{ Auth::user()->getName() }}</a>
        <a class="mr-5 col_white" href="{{ route('profile.edit') }}"><i style="color: white;" class="fas edit fa-user-cog"></i></a>
        <a class="btn mr-2 btn-outline-primary" href="{{ route('auth.signout') }}">Sign out</a>
        @else
        <a class="btn mr-2 btn-outline-primary" href="{{route('auth.signup')}}">Sign up</a>
        <a class="btn btn-outline-primary" href="{{route('auth.signin')}}">Sign in</a>
    @endif

</header>
