@if(Session::has('info'))
        <div class="alert alert-primary center" style="width: 30%" role="alert">
            {{ Session::get('info') }}
        </div>
    @endif
