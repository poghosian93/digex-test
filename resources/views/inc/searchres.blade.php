<div class="media mb-3 ">
    {{--    <a href="{{ route('profile.index', ['name' => $user->name]) }}">--}}

    <a href="{{ route('profile.index', ['name' => $users->name]) }}">
        @if(! $users->avatar)
            <img src="{{ $users->getAvatarUrl() }}" class="mr-3" alt="{{ $users->getName() }}">
        @else
            <img style="border-radius: 50%; width: 60px;" src="{{ $users->getAvatarsPath($users->id) . $users->avatar }}" class="mr-3" alt="{{ $users->getName() }}">
        @endif
{{--        <img src="{{ $users->getAvatarUrl() }}" class="mr-3" alt="{{ $users->getName() }}">--}}
    </a>
    <div class="media-body">
        <h5 class="mt-0"><a href="{{ route('profile.index', ['name' => $users->name]) }}">{{ $users->getName() }}</a></h5>
    </div>
</div>
