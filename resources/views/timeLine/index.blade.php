@extends('layouts.app')

@section('title')
    Stena
@endsection

@section('content')
    <div class="home_block">
        <div class="">
            <form method="post" action="{{ route('status.post') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <textarea name="status" class="form-control mb-3 {{ $errors->has('status')?'is-invalid': '' }}" placeholder="Do you have some new {{ Auth::user()->getName() }}?"></textarea>
                    <label for="avatar">Download Photo</label>
                    <input type="file" name="avatar" id="avatar">
                    @if($errors->has('status'))
                        <div class="invalid-feedback">
                            {{ $errors->first('status') }}
                        </div>
                        @endif
                </div>
                <button type="submit" class="btn btn_black mb-5">Share</button>
            </form>
        </div>

    <div class="">
        <div class="">
            @if(! $statuses->count())
                <p>poka net zapisei</p>
            @else
            @foreach($statuses as $status)
                <div class="mediadesk_minh pad_bt_40 mb_70">
                    <a class="mr-3" href="{{ route('profile.index', ['name'=> $status->user->name]) }}">
                        @if(! $status->user->avatar)
                            <img src="{{ $status->user->getAvatarUrl() }}" class="mr-3" alt="{{ $status->user->getName() }}">
                        @else
                            <img style="border-radius: 50%; width: 100px;" src="{{ $status->user->getAvatarsPath($status->user->id) . $status->user->avatar }}" class="mr-3" alt="{{ $status->user->getName() }}">
                        @endif
{{--                        <img class="media-object rounded" src="{{ $status->user->getAvatarUrl()}}" alt="{{ $status->user->getName() }}">--}}
                    </a>
                    <div class="media-body">
                        <h4>
                            <a href="{{ route('profile.index', ['name'=> $status->user->name]) }}">{{ $status->user->getName() }}</a>

                        </h4>
                        <div class="status_block">
                            @if(Auth::user())
                                <a title="clear" class="del_cl" href="{{ route('status.delete', ['status_id' => $status->id]) }}"><i class="fas fa-times"></i></a>
                            @endif
                            <p class="status_body">{{ $status->body }}</p>
                                @if($status->avatar)
                                    <div>
                                        <img  src="{{ $status->user->getAvatarsPath($status->user->id) . $status->avatar }}" class="mr-3 stat_img" alt="{{ $status->user->getName() }}">
                                    </div>
                                @else
                                    <div>
                                        <img style="width: 60px;" src="{{ $status->user->getAvatarUrl() }}" class="mr-3 stat_img" alt="{{ $status->user->getName() }}">
                                    </div>
                                @endif
                                <p class="font_12">{{ $status->created_at->diffForHumans() }}</p>
                                <hr>
                                <form method="POST" action="{{ route('comment.post', ['statusId' => $status->id, 'userId'=> Auth::User()->id]) }}" class="mb-4">
                                    @csrf
                                    <div class="form-group">
                                <textarea rows="3" placeholder="comment" name="reply-{{ $status->id }}" class="form-control mb-2
                                                {{ $errors->has("reply-{$status->id}") ? 'is-invalid' : '' }}">
                                </textarea>
                                        @if($errors->has("reply-{$status->id}"))
                                            <div class="invalid-feedback">
                                                {{ $errors->first("reply-{$status->id}") }}
                                            </div>
                                        @endif
                                        <button type="submit" class="btn btn_black">Comment</button>
                                    </div>
                                </form>
                                <hr>
                                @if($status->comments->count())
                                    <h1 class="text_center">Comemnt</h1>
                                @endif
                                @foreach($status->comments as $com)
                                    @if($com->status_id == $status->id && $com->reply_id == null)
                                        <hr>
                                        <div class="d-flex">
                                            <a class="mr-3" href="{{ route('profile.index', ['name'=> $com->user->name]) }}">
                                                @if(! $com->user->avatar)
                                                    <img  src="{{ $com->user->getAvatarUrl() }}" class="mr-3 com_ava" alt="{{ $com->user->getName() }}">
                                                @else
                                                    <img  src="{{$com->user->getAvatarsPath($com->user->id) . $com->user->avatar }}" class="mr-3 com_ava" alt="{{ $com->user->getName() }}">
                                                @endif
                                            </a>
                                            <div>
                                                <p class="font_24">{{ $com->user->name }}</p>
                                                <h2 class="max_body_w">{{ $com->body }}</h2>
                                                <sapn class="font_12">{{ $com->created_at->diffForHumans() }}</sapn>
                                            </div>

                                        </div>


                                        <hr>
                                        @foreach($status->comments as $co)
                                            @if($com->id == $co->reply_id)
                                                <div class="d-flex mb-3">
                                                    <a class="mr-3" href="{{ route('profile.index', ['name'=> $com->user->name]) }}">
                                                        @if(! $co->user->avatar)
                                                            <img  src="{{ $co->user->getAvatarUrl() }}" class="mr-3 com_ava" alt="{{ $co->user->getName() }}">
                                                        @else
                                                            <img  src="{{$co->user->getAvatarsPath($co->user->id) . $co->user->avatar }}" class="mr-3 com_ava" alt="{{ $co->user->getName() }}">
                                                        @endif
                                                    </a>
                                                    <div>
                                                        <p><a href="{{ route('profile.index', ['name'=> $co->user->name]) }}">{{ $co->user->name }} </a>Replied to comment</p>
                                                        <p style="display: block;" href="#">{{ $co->body }}</p>
                                                        <sapn class="font_12">{{ $co->created_at->diffForHumans() }}</sapn>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endif
                                        @endforeach
                                        <p class="otvet">Reply</p>
                                        <form method="POST" class="forms_open mb-5" action="{{ route('comment.otvet',['statusId' => $status->id,'reply' => $com->id]) }}">
                                            @csrf
                                            <input class="form-control mb-2" name="reply" placeholder="Reply">
                                            <button type="submit" class="btn btn_black">Reply</button>
                                        </form>
                                    @endif
                                @endforeach

                        </div>
                    </div>
                </div>
                @endforeach
                {{ $statuses->links() }}
            @endif
        </div>
    </div>
    </div>
@endsection
