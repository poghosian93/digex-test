<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
})->name("cont");

Route::post('/contact/submit', 'ContactController@submit')->name('contact-form');

/**
 *Login regist
 */
Route::get('/signup', 'AuthController@getSignup')->middleware('guest')->name('auth.signup');
Route::post('/signup', 'AuthController@postSignup')->middleware('guest');

Route::get('/signin', 'AuthController@getSignin')->middleware('guest')->name('auth.signin');
Route::post('/signin', 'AuthController@postSignin')->middleware('guest');

Route::get('/signout', 'AuthController@getSignout')->name('auth.signout');

/**
 * search
 */
Route::get('/search', 'SearchController@getResult')->name('search.results');

/**
 * profile
 */

Route::get('/user/{name}', 'ProfileController@getProfile')->name('profile.index');
Route::get('/profile/getAllProfile', 'ProfileController@getAllProfile')->name('profile.allProfile');
Route::post('/upload-avatar/{name}', 'ProfileController@postUploadAvatar')->name('upload.avatar');
Route::get('/delete-avatar/{user_id}', 'ProfileController@getDeleteAvatar')->name('profile.avatarDelete');

/**
 * edit
 */
Route::get('/profile/edit', 'ProfileController@getEdit')->middleware('auth')->name('profile.edit');
Route::post('/profile/edit', 'ProfileController@postEdit')->middleware('auth')->name('profile.edit');
# post
Route::post('/status', 'StatusController@postStatus')->middleware('auth')->name('status.post');

#comment

Route::post('/comment/{statusId}', 'CommentController@postReply')->middleware('auth')->name('comment.post');
Route::post('/comment/{statusId}/{reply}', 'CommentController@postSend')->middleware('auth')->name('comment.otvet');
Route::get('/status/{status_id}', 'StatusController@getDeleteStatus')->middleware('auth')->name('status.delete');

#messages
Route::post('/messages/{friend_id}', 'MessagesController@postSendMessage')->middleware('auth')->name('messages.send');

#friends
Route::get('/friends', 'FriendController@getIndex')->middleware('auth')->name('friend.index');

Route::get('/friends/add/{username}', 'FriendController@getAdd')->middleware('auth')->name('friend.add');

Route::get('/friends/accept/{username}', 'FriendController@getAccept')->middleware('auth')->name('friend.accept');

Route::post('/friends/delete/{username}', 'FriendController@postDelete')->middleware('auth')->name('friend.delete');
